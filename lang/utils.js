const 
  git = require('simple-git')('./'),
  fs = require('fs-extra'),
  chalk = require('chalk'),
  exec = require('child_process').exec;
;

const 
  REMOTE = `https://oauth2:NNMkYCA9vscL6D4YJtGP@gitlab.com/jdujaka1/saltoks.git`,
  EMAIL = `jdujaka@gmail.com`,
  USERNAME = `jdujaka1`
;


const initRemote = async () => {
  try {
    await git.listRemote(['--heads', '--tags'], (e, res) => {
      if(e){
        console.log(e);
        return;
      }
      if(!res.length) {
        git.addRemote('origin', REMOTE);
      }
  });
  } catch(e) {
    console.log(e);
  }
}

const pullFromGit = () => {
  return new Promise(async (resolve, reject)=>{

    console.log(chalk.bgCyan.white(`Pulling from git...`));

    await initRemote();

    git.pull('origin', 'master', (e) => {
      if(e) {
        console.log(chalk.bgRed.white(`Something went wrong ${e}`));
        reject(e);
      }
      console.log(chalk.bgYellow.white(`Pulled from git!`));
      resolve();
    });
  }); 
}

const resetRepo = () => {
  git.reset('hard', () => console.log('repo reseted'));
}

const pushToGit = () => {
  return new Promise(async (resolve, reject) => {
    console.log(chalk.bgCyan.white(`Initialising git...`));

    git
    .add('./lang/languages/**.*')
    .commit("Commit made from language export script")
    .push(['origin', 'master'], 
      (e, res) => {
        if(e) reject(e);
        console.log(chalk.bgGreen.white(`Everything pushed to git!`)); 
        resolve()
      }
    );
  })
}

const cleanUp = async dir => {
  try {
    await fs.remove(dir);
    console.log(chalk.bgBlue.white("Everything tidy!"));
  } catch(e) {
    console.log(chalk.bgRed.white(`Something went wrong during cleanup: ${e}`))
  }
  return;
}

export {
  pullFromGit,
  pushToGit,
  cleanUp,
  resetRepo
}