import * as SB from './storyblok';
import * as UTIL from './utils';

const 
  fs = require("fs-extra"),
  path = require("path"),
  chalk = require("chalk"),
  _ = require("lodash")
;

const 
  export_dir = 'languages',
  i18nComponents = [],
  i18nContent = []
; 

const parseComponents = components => {
  return new Promise((resolve, reject)=> {
    const promises = [];
    components.map(component => promises.push(
        parseComponent(component)
      )
    );

    Promise.all(promises).then(()=>{
      resolve('All components parsed');
    }).catch(e => reject(e));
  });
}

const parseComponent = (component) => {
  let i18nComponent = {
    id: component.id,
    name: component.name,
    schema: []
  };
  const schema = component.schema;

  Object.keys(schema).map(item => {
    if(schema[item].translatable){
      i18nComponent.schema.push(item);
    }
  });

  // Only save the component if it has translatable schema
  if(Object.entries(i18nComponent.schema).length !== 0) {
    i18nComponents.push(i18nComponent);
  }

};

const initStoryContent = async (id) => {
  return new Promise(async (resolve, reject) => {
    try {
      const storyData = await SB.getStoryContent(id);
      await getAllTranslateableFields(storyData.data.story);
      resolve();
    } catch (e) {
      reject(e);
    }
  })
}

const getAllTranslateableFields = (story) => {
  const content = story.content;

  return new Promise(resolve => {
    let i18nStory = {
      id: story.id,
      name: story.name,
    }
    const i18nComponent = i18nComponents.find(component => {
      return component.name === content.component
    });

    const i18nFields = extractComponent(i18nComponent, content)
    i18nStory.content = i18nFields;
    i18nContent.push(i18nStory);
    resolve();
  });
};

const extractComponent = (i18nComponent, content) => {
  const contentKeys = Object.keys(content);
  let result = {};
  contentKeys.map(key => {
    if(Array.isArray(content[key])){

      result[key] = [];
      content[key].map(field => {

        const i18nComponent = i18nComponents.find(component => {
          return component.name === field.component
        });

        if(i18nComponent){
          result[key].push(extractComponent(i18nComponent, field));
        }
      })

    } else if(i18nComponent && i18nComponent.schema.includes(key) && content[key].length){
      result[key] = content[key];
    }
  });
  return result;
}

const writeFile = (file, content) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(`lang/${export_dir}/${file}.json`, content, "utf8", function(err) {
      if (err) {
        reject(`Couldn't write files` + err);
      }
      console.log(chalk.bgGreen.white(`Exported ${file}.json`));
      resolve();
    });
  })
}

const exportToFile = allContent => {
  return new Promise(async (resolve, reject) => {

    const dir = `lang/${export_dir}`;

      try {
        await fs.emptyDir(dir)
        console.log(chalk.bgYellow.white(`Directory ensured!`));
      } catch (err) {
        console.error(err)
      }

    // fs.existsSync(dir) ? await emptyDir(dir) : fs.mkdirSync(dir);

    const promises = [];

    allContent.map(story => {

      const content = JSON.stringify(story, undefined, 2);
      promises.push(writeFile(story.name, content));
    });

    await Promise.all(promises);
    resolve();
  });
};

const init = async () => {
  try {
    console.log(chalk.bgCyan.white(`Getting all components...`));
    const allComponents = await SB.getAllComponents();

    await parseComponents(allComponents.data.components);
    console.log(chalk.bgYellow.white(`All components parsed!`));

    const allStories = await SB.getAllStories();

    const promises = [];
    allStories.data.stories.forEach(story => {
      i18nContent[story.name] = {};
      promises.push(initStoryContent(story.id));
    });

    await Promise.all(promises);
    
    await exportToFile(i18nContent);
    await UTIL.pushToGit();

    console.log(chalk.bgWhite.blue("Finished everything successfully"));

  } catch (e) {
    console.log(chalk.bgRed.white(`Something went wrong: ${e}`));
  } finally {
    console.log(chalk.bgWhite.black("Process complete"));
  }

}

init();

export default init;