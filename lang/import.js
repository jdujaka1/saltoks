import * as SB from './storyblok';
import * as UTIL from './utils';

const DIR = `./lang/languages/nl`;

const 
  fs = require("fs-extra"),
  path = require("path"),
  chalk = require("chalk"),
  _ = require("lodash") 
;

const 
  existingStories = [],
  updatedStories = []
;

const parseStory = (story) => {

  return new Promise((resolve, reject) => {
    if(story.id){
      existingStories.map(item => {
        const oldItem = item.data.story
        if(oldItem.id === story.id){
          const lang = story.lang;
          let content = {};
          if(lang !== 'en'){
            content = getContent(story.content)
          }
          _.merge(item.data.story.content, content);
        }
      });
    }
    resolve();
  })
}

const getContent = content => {
  if(!content) return;
  const contentKeys = Object.keys(content);
  let result = {};

  contentKeys.map(key => {
    if(Array.isArray(content[key])){
      result[key] = [];
      content[key].map(item => {
        result[key].push(getContent(item));
      })
    } else {
      result[`${key}__i18n__nl`] = content[key];
    }
  });

  return result;
}

const readFile = filePath => {
  return new Promise( async (resolve, reject) => {
    console.log(chalk.bgCyan.white(`Reading file: ${filePath}`));
    try {
      const story = JSON.parse(fs.readFileSync(filePath, "utf8"));
      if(Object.entries(story).length === 0) {
        resolve();
      }
      await parseStory(story);
      resolve();
    } catch (e) {
      reject(filePath + ' - ' + e);
    }
  });
}

const readDir = (dirPath) => {
  return new Promise((resolve, reject) => {
    console.log(chalk.bgCyan.white(`Reading directory: ${dirPath}`));
    const promises = [];

    // Filter out files/directories starting with a dot (.git, .gitlog)
    const dirItems = fs.readdirSync(dirPath).filter(item => {
      return !item.startsWith('.') && item !== 'crowdin.yml';
    });;

    dirItems.map(item => {
      const itemPath = path.join(dirPath, item);
      const isDir = fs.statSync(itemPath).isDirectory();
      promises.push(isDir ? readDir(itemPath) : readFile(itemPath));
    });

    Promise.all(promises).then((result) => {
      console.log(chalk.bgYellow.white(`All stories read successfully!`));
      resolve(`Read all files successfully!`);
    }).catch(e => {
      reject(e);
    })


  });
}

const init = async () => {

  try {
    const stories = await SB.getExistingStoriesContent();
    existingStories.push(...stories);
    await UTIL.pullFromGit();
    await readDir(DIR);
    console.log(chalk.bgCyan.white(`Updating all the stories in Storyblok...`));
    await SB.updateStories(existingStories);
    console.log(chalk.bgWhite.blue("Finished everything successfully"));
  } catch (e) {
    console.log(chalk.bgRed.white(`Something went wrong: ${e}`));
  } finally {
    console.log(chalk.bgWhite.black("Process complete!"));
  }

}

init();

export default init;



