import importLang from './import';
import exportLang from './export';

const gitData = JSON.parse(process.env.INCOMING_HOOK_BODY);

console.log('Checking for import/export triggers');

if(gitData.action === 'published') {
  console.log('Init export of languages...');
  exportLang();
} else if (gitData.commits && gitData.commits[0].message.includes('New translations')){
  console.log('Init import of translations...');
  importLang();
} else if(gitData.commits && gitData.commits[0].message.includes('no_build')){
  console.log('Build prevented by commit message!')
  console.log('Exiting...')
  process.exit(1);
} else {
  console.log('No triggers for import/export, continuing with build');
}

