const 

  // Story block constants
  StoryblokClient = require("storyblok-js-client"),
  oauthToken = "fZN4B5E8hGAgR9r8quSinwtt-42615-_3gsT4PiznM3zjXEkuwN",
  spaceId = "66237", // id of space where to swap content
  
  Storyblok = new StoryblokClient({
    oauthToken
  }),

  //Other
  chalk = require("chalk"),
  fs = require("fs")
;

const getAllComponents = () => {
  return new Promise((resolve, reject) => {
    Storyblok.get(`spaces/${spaceId}/components`, {})
      .then(response => {

        fs.writeFile(`lang/components/components.json`, JSON.stringify(response), "utf8", function(err) {
            if (err) {
              reject(`Couldn't write file` + err);
            }
          });
        resolve(response);
      })
      .catch(e => reject(e));
  });
};

const getExistingStoriesContent = () => {
  return new Promise(async (resolve, reject) => {
    console.log(chalk.bgCyan.white(`Getting all stories from the CMS...`));
    const storiesList = await getAllStories();
    const stories = storiesList.data.stories;

    const result = [];

    Promise.all(
      stories.map(async story => {
        try {
          const storyData = await getStoryContent(story.id, story.name);
          return storyData;
        } catch (e) {
          reject(e);
        }
      })
    ).then(result => {
      resolve(result);
    })
  })
  
}

const updateStories = (existingStories) => {
  return new Promise((resolve, reject) => {
    const promises = [];
    existingStories.map(story => {
      promises.push(updateStory(story));
    });

    Promise.all(promises).then(()=> {
      resolve();
    }).catch(e => reject(e));

  });
}

const updateStory = story => {
  return new Promise((resolve, reject) => {
    Storyblok.put(`spaces/${spaceId}/stories/${story.data.story.id}`, {
      story: story.data.story,
      publish: 1
    }).then((response) => {
      console.log(chalk.bgYellow.white(`Updated successfully: ${response.data.story.name}`));
      resolve();
    }).catch(e => console.log(e));
  })
}


const getStoryContent = (storyId, name = '') => {
  return new Promise((resolve, reject) => {
    if(name){
      console.log(chalk.bgCyan.white(`Fetching ${name}`));
    }
    Storyblok.get(`spaces/${spaceId}/stories/${storyId}`, {})
      .then(response => {

        console.log(chalk.bgYellow.white(`Succesfully fetched ${response.data.story.name}`));
        fs.writeFile(`lang/stories/${response.data.story.name}.json`, JSON.stringify(response.data.story), "utf8", function(err) {
          if (err) {
            reject(`Couldn't write file` + err);
          }
          console.log(chalk.bgGreen.white(`Exported ${response.data.story.name}.json`));
          resolve();
        });
        resolve(response);
      })
      .catch(e => reject(e));
  });
};

const getAllStories = () => {
  return new Promise((resolve, reject) => {
    Storyblok.get(`spaces/${spaceId}/stories`, {})
      .then(response => {
        resolve(response);
      })
      .catch(e => reject(e));
  });
};


export {
  getAllComponents,
  getExistingStoriesContent,
  updateStories,
  getStoryContent,
  getAllStories
}