const axios = require('axios');

const $RefParser = require("json-schema-ref-parser");
const fs = require("fs");

const getJson = async (endpoint) => {
  try {
    return await axios.get(endpoint);
  } catch(e){
    console.log(e);
  }
}

const init = async (endpoint) => {
  console.log('Getting API documentation from: ' + endpoint)
  const jsonObject = await getJson(endpoint);

  let out = '';

  try { 
    let content = await $RefParser.dereference(jsonObject.data);
    return JSON.stringify(content.paths);
  }
  catch(err) {
    console.error(err);
  }
};

export default init;