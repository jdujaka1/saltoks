const transformImage = (image, option) => {
  if (!image) return ''

  let imageService = 'https://img2.storyblok.com/'
  let path = image.replace('//a.storyblok.com', '')
  return imageService + option + path
}

export {
  transformImage
};