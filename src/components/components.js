import Page from './page'
import Grid from './grid'
import Teaser from './teaser'
import Title from './title'
import Hero from './hero'
import Feature from './feature'
import Endpoint from './endpoint'
import Content from './content'
import Features from './features'
import TextBlock from './textBlock'
import List from './list'
import Cards from './cards'
import Card from './card'
import ComponentNotFound from './component_not_found'

const ComponentList = {
  page: Page,
  title: Title,
  grid: Grid,
  teaser: Teaser,
  feature: Feature,
  endpoint: Endpoint,
  hero: Hero,
  content: Content,
  features: Features,
  textblock: TextBlock,
  cards: Cards,
  card: Card,
  list: List
}

const Components = (type) => {
  if(!type){
    return ComponentNotFound
  }
  const adjType = type.toLowerCase();
  if (typeof ComponentList[adjType] === 'undefined') {
    return ComponentNotFound
  }
  return ComponentList[adjType]
}

export default Components