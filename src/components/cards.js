import React from "react";
import Card from "./card.js";

const Cards = ({blok}) => {

  return (
    <div class="row cards">
      {blok.card && blok.card.map(card => (
        <Card card={card} />
      ))}
    </div>
  );

}

export default Cards;