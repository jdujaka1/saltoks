import React from "react"
import Link from "gatsby-link"
import SbEditable from "storyblok-react"
import { transformImage } from "../utils"

const Hero = props => (
  <SbEditable content={props.blok}>
      <div className="row content-item justify-content-lg-center pull-up">
        <div className={`col-xl-6 mx-auto text-center ${props.blok.class}`}>
          <h3>{props.blok.pretitle}</h3>
          <h1>{props.blok.title}</h1>
          <p className="intro">{props.blok.subtitle}</p>
          <img 
            className="img-fluid"
            src={transformImage(props.blok.image, '800x0/filters:format(webp)')} 
          />
        </div>
      </div>
  </SbEditable>
)

export default Hero
