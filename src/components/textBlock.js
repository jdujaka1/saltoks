import React from 'react';

const TextBlock = ({blok}) => {

  const {
    pretitle,
    classes,
    title,
    paragraph
  } = blok;

  return (
    <div className={classes}>
      
    {pretitle && <h3>{pretitle}</h3>}
    {title && <h1>{title}</h1>}
    {paragraph && <p class="intro">{paragraph}</p>}

    </div>
  )
}

export default TextBlock;