import React, { useState } from "react"
import SbEditable from "storyblok-react"
import Img from "gatsby-image"
import { transformImage } from "../utils";
import { getFluidGatsbyImage } from "gatsby-storyblok-image"

const Features = ({blok}) => {
  
  const [activeFeature, setActiveFeature] = useState(0);

  return (
    <SbEditable content={blok}>
      <div className="row content-item">
        <div className="col-12 col-md-6 col-lg-4 offset-lg-1">
          <h2>{blok.title}</h2>
          <p className="intro mb-5">{blok.subtitle}</p>

          {blok.features && blok.features.map((feature, i) => {
            return (
              <div 
                className={`highlight ${i === activeFeature ? 'active' : ''}`}
                onMouseOver={()=>setActiveFeature(i)}
                key={`feature-${i}`}
              >
                  <div className={`icon ${i === activeFeature ? 'active' : ''}`}>
                    <i className="fa fa-check"></i>
                  </div>
                <div className="highlight-content border-bottom">
                  <h3 className="no-decoration">{feature.title}</h3>
                  <p>
                    {feature.description}
                  </p>
                </div>
              </div>
            )
          })}
        </div>
        <div className="col-12 col-md-6 col-lg-6 offset-lg-1 text-center d-none d-md-block">
        {blok.features && blok.features.map((feature, i) => {
          if(i === activeFeature){
            return featureImage(feature)
          }
        })}
        </div>
      </div>
    </SbEditable>
  )
}

const featureImage = (feature) => {
  console.log(feature);
  return (
    <div className="highlight-slide">
    {feature.main_image && (
      <img 
        className="img-fluid"
        src={transformImage(feature.main_image, '300x0/filters:format(webp)')} 
      />
    )}

    {feature.bg_image && (
      <img 
        className="img-fluid go-back go-back-lg left d-none d-lg-block"
        src={transformImage(feature.bg_image, '670x0/filters:format(webp)')} 
      />
    )}
    </div>
  )
}

export default Features
