import React from 'react'
import Components from './components.js';
import SbEditable from 'storyblok-react'

const Title = (props) => (
  <SbEditable content={props.blok}>
    <h1>{props.blok.title}</h1>
  </SbEditable>
)

export default Title