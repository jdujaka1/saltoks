import React from "react";

const DevMenu = () => (
  <>
    <nav className="developers-menu border-none">
      <h4 className="dev-gray-headline">Getting Started</h4>
      <ul className="nav navbar-nav list-decimal">
        <li
          id="menu-item-8162"
          className="menu-item menu-item-type-post_type menu-item-object-page menu-item-8162"
        >
          <a href="https://staging.saltoks.com/developers/connect/welcome/">
            Welcome
          </a>
        </li>
        <li
          id="menu-item-9158"
          className="menu-item menu-item-type-post_type menu-item-object-page menu-item-9158"
        >
          <a href="https://staging.saltoks.com/developers/connect/connect-api-salto-ks/">
            Connect API &amp; SALTO KS
          </a>
        </li>
        <li
          id="menu-item-8161"
          className="menu-item menu-item-type-post_type menu-item-object-page menu-item-8161"
        >
          <a href="https://staging.saltoks.com/developers/connect/getting-authenticated/">
            Getting authenticated
          </a>
        </li>
        <li
          id="menu-item-8160"
          className="menu-item menu-item-type-post_type menu-item-object-page menu-item-8160"
        >
          <a href="https://staging.saltoks.com/developers/connect/setting-up-a-site/">
            Setting up a site
          </a>
        </li>
        <li
          id="menu-item-8159"
          className="menu-item menu-item-type-post_type menu-item-object-page menu-item-8159"
        >
          <a href="https://staging.saltoks.com/developers/connect/adding-and-activating-an-iq/">
            Adding and activating an IQ
          </a>
        </li>
        <li
          id="menu-item-9164"
          className="menu-item menu-item-type-post_type menu-item-object-page menu-item-9164"
        >
          <a href="https://staging.saltoks.com/developers/connect/generating-otp/">
            Generating OTP
          </a>
        </li>
        <li
          id="menu-item-8158"
          className="menu-item menu-item-type-post_type menu-item-object-page menu-item-8158"
        >
          <a href="https://staging.saltoks.com/developers/connect/setting-up-a-lock/">
            Setting up a lock
          </a>
        </li>
        <li
          id="menu-item-8157"
          className="menu-item menu-item-type-post_type menu-item-object-page menu-item-8157"
        >
          <a href="https://staging.saltoks.com/developers/connect/adding-tags/">
            Adding tags
          </a>
        </li>
        <li
          id="menu-item-8156"
          className="menu-item menu-item-type-post_type menu-item-object-page menu-item-8156"
        >
          <a href="https://staging.saltoks.com/developers/connect/granting-a-user-access-to-a-lock/">
            Granting a user access to a lock
          </a>
        </li>
        <li
          id="menu-item-9172"
          className="menu-item menu-item-type-post_type menu-item-object-page menu-item-9172"
        >
          <a href="https://staging.saltoks.com/developers/connect/remote-lock-opening/">
            Remote Lock Opening
          </a>
        </li>
      </ul>
    </nav>
    <nav className="developers-menu border-none">
      <h4 className="dev-gray-headline">Authentication</h4>
      <ul className="nav navbar-nav list-decimal">
        <li
          id="menu-item-6993"
          className="menu-item menu-item-type-post_type menu-item-object-page contracted"
        >
          <a href="https://staging.saltoks.com/developers/connect/introduction/">
            Introduction
          </a>
        </li>
        <li
          id="menu-item-6992"
          className="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children contracted"
        >
          <a href="https://staging.saltoks.com/developers/connect/openid-connect-concepts/">
            OpenID Connect concepts
          </a>
          <span className="expand">+</span>
          <ul className="sub-menu">
            <li
              id="menu-item-6996"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/application-types/">
                Application Types
              </a>
            </li>
            <li
              id="menu-item-6991"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/tokens/">
                Tokens
              </a>
            </li>
            <li
              id="menu-item-6994"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/endpoints/">
                Endpoints
              </a>
            </li>
            <li
              id="menu-item-6995"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/authorize-endpoint/">
                Authorize Endpoint
              </a>
            </li>
          </ul>
        </li>
        <li
          id="menu-item-7290"
          className="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children contracted"
        >
          <a href="https://staging.saltoks.com/developers/connect/openid-connect-flows/">
            OpenID Connect flows
          </a>
          <span className="expand">+</span>
          <ul className="sub-menu">
            <li
              id="menu-item-7288"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/browser-based-applications/">
                Browser-based Applications
              </a>
            </li>
            <li
              id="menu-item-7287"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/silent-refresh/">
                Silent Refresh
              </a>
            </li>
            <li
              id="menu-item-7286"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/mobile-applications/">
                Mobile applications
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </nav>
    <nav className="developers-menu">
      <h4 className="dev-gray-headline">Resources</h4>
      <ul className="nav navbar-nav">
        <li
          id="menu-item-9478"
          className="menu-item menu-item-type-custom menu-item-object-custom menu-item-9478"
        >
          <a href="/developers/connect/glossary">Glossary</a>
        </li>
      </ul>
    </nav>
    <nav className="developers-menu">
      <h4 className="dev-gray-headline">Endpoints</h4>
      <ul className="nav navbar-nav">
        <li
          id="menu-item-8543"
          className="menu-item menu-item-type-post_type menu-item-object-page contracted"
        >
          <a href="https://staging.saltoks.com/developers/connect/glossary/">
            Glossary
          </a>
        </li>
        <li
          id="menu-item-7193"
          className="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children contracted"
        >
          <a href="#">Sites</a>
          <span className="expand">+</span>
          <ul className="sub-menu">
            <li
              id="menu-item-7194"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/get-sites/">
                Get sites
              </a>
            </li>
            <li
              id="menu-item-7195"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/create-a-site/">
                Create a site
              </a>
            </li>
          </ul>
        </li>
        <li
          id="menu-item-7196"
          className="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children contracted"
        >
          <a href="#">IQs</a>
          <span className="expand">+</span>
          <ul className="sub-menu">
            <li
              id="menu-item-7198"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/get-iqs/">
                Get IQs
              </a>
            </li>
            <li
              id="menu-item-7199"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/get-iq/">
                Get IQ
              </a>
            </li>
            <li
              id="menu-item-7202"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/activate-iq/">
                Activate IQ
              </a>
            </li>
            <li
              id="menu-item-7204"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="http://staging.saltoks.com/developers/connect/get-hardwares-on-iq/">
                Get hardware(s) on IQ
              </a>
            </li>
            <li
              id="menu-item-7203"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/set-hardware-tree-of-iq/">
                Set hardware tree of IQ
              </a>
            </li>
          </ul>
        </li>
        <li
          id="menu-item-7206"
          className="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children contracted"
        >
          <a href="#">Locks</a>
          <span className="expand">+</span>
          <ul className="sub-menu">
            <li
              id="menu-item-7212"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/add-a-lock/">
                Add a lock
              </a>
            </li>
            <li
              id="menu-item-7219"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/unlock/">
                Unlock
              </a>
            </li>
            <li
              id="menu-item-7213"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/add-tags/">
                Add tags
              </a>
            </li>
            <li
              id="menu-item-7216"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/detach-lock-from-iq/">
                Detach lock from IQ
              </a>
            </li>
            <li
              id="menu-item-7207"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/get-locks/">
                Get locks
              </a>
            </li>
            <li
              id="menu-item-7218"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/get-lock/">
                Get lock
              </a>
            </li>
            <li
              id="menu-item-7221"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/set-update-easy-office-mode/">
                Set / update Easy Office Mode
              </a>
            </li>
            <li
              id="menu-item-7217"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/get-easy-office-mode/">
                Get Easy Office Mode
              </a>
            </li>
            <li
              id="menu-item-7214"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/delete-easy-office-mode/">
                Delete Easy Office Mode
              </a>
            </li>
          </ul>
        </li>
        <li
          id="menu-item-7222"
          className="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children contracted"
        >
          <a href="#">Repeaters</a>
          <span className="expand">+</span>
          <ul className="sub-menu">
            <li
              id="menu-item-7224"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/add-repeaters/">
                Add repeaters
              </a>
            </li>
            <li
              id="menu-item-7223"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/get-repeaters/">
                Get repeaters
              </a>
            </li>
          </ul>
        </li>
        <li
          id="menu-item-7225"
          className="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children contracted"
        >
          <a href="#">Tags</a>
          <span className="expand">+</span>
          <ul className="sub-menu">
            <li
              id="menu-item-7227"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/get-tags/">
                Get tags
              </a>
            </li>
            <li
              id="menu-item-7228"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/block-unblock-tags/">
                Block/Unblock tags
              </a>
            </li>
            <li
              id="menu-item-7215"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/delete-tags/">
                Delete tags
              </a>
            </li>
          </ul>
        </li>
        <li
          id="menu-item-7229"
          className="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children contracted"
        >
          <a href="#">Users</a>
          <span className="expand">+</span>
          <ul className="sub-menu">
            <li
              id="menu-item-7231"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/search-users/">
                Search users
              </a>
            </li>
          </ul>
        </li>
        <li
          id="menu-item-7230"
          className="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children contracted"
        >
          <a href="#">Site users</a>
          <span className="expand">+</span>
          <ul className="sub-menu">
            <li
              id="menu-item-7232"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/get-users/">
                Get users
              </a>
            </li>
            <li
              id="menu-item-7233"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/add-invite-a-user/">
                Add/Invite a user
              </a>
            </li>
            <li
              id="menu-item-7234"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/add-existing-user/">
                Add existing user
              </a>
            </li>
            <li
              id="menu-item-7235"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/edit-user/">
                Edit user
              </a>
            </li>
            <li
              id="menu-item-7236"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/delete-user/">
                Delete user
              </a>
            </li>
          </ul>
        </li>
        <li
          id="menu-item-7237"
          className="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children contracted"
        >
          <a href="#">Site Access Groups</a>
          <span className="expand">+</span>
          <ul className="sub-menu">
            <li
              id="menu-item-7244"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/get-access-groups/">
                Get access groups
              </a>
            </li>
            <li
              id="menu-item-7246"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/add-an-access-group/">
                Add an access group
              </a>
            </li>
            <li
              id="menu-item-7239"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/update-an-access-group/">
                Update an access group
              </a>
            </li>
            <li
              id="menu-item-7247"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/delete-an-access-group/">
                Delete an access group
              </a>
            </li>
            <li
              id="menu-item-7242"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/get-time-schedules-for-an-access-group/">
                Get time schedules for an access group
              </a>
            </li>
            <li
              id="menu-item-7248"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/add-a-time-schedule-for-an-access-group/">
                Add a time schedule for an access group
              </a>
            </li>
            <li
              id="menu-item-7238"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/update-time-schedules-for-an-access-group/">
                Update time schedules for an access group
              </a>
            </li>
            <li
              id="menu-item-7245"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/delete-time-schedules-for-an-access-group/">
                Delete time schedules for an access group
              </a>
            </li>
            <li
              id="menu-item-7241"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/get-users-in-an-access-group/">
                Get users in an access group
              </a>
            </li>
            <li
              id="menu-item-7249"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/add-a-user-to-an-access-group/">
                Add a user to an access group
              </a>
            </li>
            <li
              id="menu-item-7250"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/delete-a-user-from-an-access-group/">
                Delete a user from an access group
              </a>
            </li>
            <li
              id="menu-item-7252"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/add-remove-multiple-users-to-an-access-group/">
                Add/remove multiple users to an access group
              </a>
            </li>
            <li
              id="menu-item-7243"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/get-current-locks-in-the-access-group/">
                Get current locks in the access group
              </a>
            </li>
            <li
              id="menu-item-7251"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/add-a-lock-to-an-access-group/">
                Add a lock to an access group
              </a>
            </li>
            <li
              id="menu-item-7259"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/delete-a-lock-from-an-access-group/">
                Delete a lock from an access group
              </a>
            </li>
            <li
              id="menu-item-7240"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/remove-locks-from-an-access-group/">
                Remove locks from an access group
              </a>
            </li>
          </ul>
        </li>
        <li
          id="menu-item-7253"
          className="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children contracted"
        >
          <a href="#">Entries</a>
          <span className="expand">+</span>
          <ul className="sub-menu">
            <li
              id="menu-item-7254"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/entries/">
                Entries
              </a>
            </li>
            <li
              id="menu-item-7255"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/incidents/">
                Incidents
              </a>
            </li>
          </ul>
        </li>
        <li
          id="menu-item-7256"
          className="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children contracted"
        >
          <a href="#">Roles</a>
          <span className="expand">+</span>
          <ul className="sub-menu">
            <li
              id="menu-item-7257"
              className="menu-item menu-item-type-post_type menu-item-object-page contracted"
            >
              <a href="https://staging.saltoks.com/developers/connect/get-roles/">
                Get roles
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </nav>
    <nav className="developers-menu">
      <h4 className="dev-gray-headline">Mobile SDK</h4>
      <ul className="nav navbar-nav">
        <li
          id="menu-item-7361"
          className="menu-item menu-item-type-post_type menu-item-object-page menu-item-7361"
        >
          <a href="http://staging.saltoks.com/developers/connect/mobile-sdk-introduction/">
            Introduction
          </a>
        </li>
        <li
          id="menu-item-7360"
          className="menu-item menu-item-type-post_type menu-item-object-page menu-item-7360"
        >
          <a href="https://staging.saltoks.com/developers/connect/ios-reference/">
            iOS reference
          </a>
        </li>
        <li
          id="menu-item-7359"
          className="menu-item menu-item-type-post_type menu-item-object-page menu-item-7359"
        >
          <a href="https://staging.saltoks.com/developers/connect/android-reference/">
            Android reference
          </a>
        </li>
      </ul>
    </nav>
  </>
);

export default DevMenu;
