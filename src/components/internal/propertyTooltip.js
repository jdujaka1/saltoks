import React, {useState} from "react"
import ReactMarkdown from 'react-markdown';
import SbEditable from "storyblok-react"

const PropertyTooltip = ({property}) => {

  const [tooltip, toggleTooltip] = useState(false);

  return (
    <span>
      {tooltip && (<span className="tooltip">
          <h6>{property.name}</h6>
          <span>type: {property.type}, format: {property.format};</span>
          <span></span>
          {property.required && <span className="text-blue"><br/>required</span> }
          <p>{property.description}</p>
        </span>
      )}
      <span onMouseEnter={() => toggleTooltip(true)} onMouseLeave={() => toggleTooltip(false)} className="pointer tooltip-handel">{`${property.name}`}</span>
    </span>
  )
}

export default PropertyTooltip
