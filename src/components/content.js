import React from "react"
import ReactMarkdown from 'react-markdown';
import SbEditable from "storyblok-react"

const Content = ({blok}) => {
  return (
    <SbEditable content={blok}>
      <ReactMarkdown source={blok.content} />
    </SbEditable>
  )
}

export default Content
