import React from "react"
import SbEditable from "storyblok-react"
import Img from "gatsby-image"
import { getFluidGatsbyImage } from "gatsby-storyblok-image"

const Feature = ({blok}) => {

  const renderImage = () => {
    const fluidProps = getFluidGatsbyImage(blok.image, {
      maxWidth: 200,
    })
    return <Img fluid={fluidProps} />
  }
  

  return (
    <SbEditable content={blok}>
      <div className="col-4">
        <h2>{blok.name}</h2>
      </div>
    </SbEditable>
  )
}

export default Feature
