import React from 'react'
import Components from './components.js';
import Layout from './layout.js';

const Page = (props) => {
  return(
    <Layout classes="developers" slug={props.slug} siblings={props.blok.siblings}>
      {props.blok.body && props.blok.body.map((blok) => React.createElement(Components(blok.component), {key: blok._uid, blok: blok}))}
    </Layout>
  )
}

export default Page