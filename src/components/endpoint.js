import React, {useState} from "react";
import SbEditable from "storyblok-react";
import Layout from "./layout";
import Content from "./content";
import DevMenu from "./internal/devmenu";
import PropertyTooltip from "./internal/propertyTooltip";
import { Tooltip } from 'react-tippy';

import { graphql } from "gatsby"
  
const Endpoint = ({blok, slug}) => {
  
  const [activeTab, setActiveTab] = useState(0);
  const endpointData = {};

  if(blok.Endpoint.data){
    Object.assign(endpointData, {
      parameters: blok.Endpoint.data.parameters
    })
  }

  return (
    <SbEditable content={blok}>
      <Layout classes="developers" slug={slug} siblings={blok.siblings}>
        <div className="row">
          <div className="col-md-3 col-dev-lg-2">
            <DevMenu />
          </div>

          {endpointData.hasOwnProperty('parameters') ? 
            endpointLayout(blok, activeTab, setActiveTab) : contentLayout(blok)
          }

        </div>
      </Layout>
    </SbEditable>
  )
}

const property = (prop, isSchema) => {
  return (
  <>
    <span className="text-capitalize text-white">{prop.in} </span>
    {/*&#123;
    <PropertyTooltip property={prop} />
    &#125; */}
    <span className="text-white">{`(${prop.type})`}</span> 
    {/*{!prop.required && <span className="text-red">required</span>}*/}
    <br/>
  </>
)
}

const endpointLayout = (blok, activeTab, setActiveTab) => {
  const endpoint = blok.Endpoint.data;

  return (
    <div className="col-xs-12 col-dev-lg-10 col-md-9 box-light-gray pr-0 pl-0">
      <div className="row h-100 flex-lg-row">
        <div className="col-lg-4 content">
          <Content blok={blok} />
        </div>
        <div className="col-lg-8 code pr-0 pl-0">
          <div className="col-md-1 line-numbers mt-2">
              <pre>{[...Array(100)].map((e,i) => <>{i}<br/></>)}</pre>
          </div>

          <div className="tab-header mt-2">
            <a onClick={() => setActiveTab(0)} className={`btn header-response ${activeTab === 0 ? 'btn-primary' : 'btn-default'}` }>request</a>
            {Object.keys(endpoint.responses).map((code,i) => (
              <a onClick={() => setActiveTab(code)} className={`btn header-response ml-2 ${activeTab === code ? 'btn-primary' : 'btn-default'}`}>Response {code}</a>
            ))}
          </div>
          <div class="tab-body">
            {activeTab === 0 ? 
              (
                <pre class="col-md-11">
                  Endpoint: <span>{blok.Endpoint.endpoint}</span><br/>
                  Method: <span>{blok.Endpoint.method}</span><br/>
                  Parameters: <br/>
                  {endpoint.hasOwnProperty('parameters') && endpoint.parameters.map((p,i) =>
                    p.schema 
                      ? Object.keys(p.schema.properties).map(prop => property(
                        {
                          in: prop, 
                          type: p.schema.properties[prop].type
                        }
                      )) : property(p)
                  )}
                </pre>
              ) : responseDetails(endpoint.responses[activeTab])
            }
          </div>
        </div>
      </div>
    </div>
  );
}

const renderParameters = endpoint => {

}

const responseDetails = (response) => {

  let props = {};
  if(response.schema.items) {
     props = response.schema.items.properties;
  } else if(response.schema.properties.items) {
     props = response.schema.properties.items;
  } else {
     props = response.schema.properties
  }
  const propsArray = Object.keys(props);

  return (<pre class="col-md-11">
    Response Content Type (application/json):
    [<br/>
      &#123;<br/>

      {propsArray.map((key, i) => (
        <>
          <span>
            <span>"{key}"</span>
          </span>
          <span className="text-white"> : </span>
          <span>"{props[key].type}"</span>
          {i !== propsArray.length-1 && <span className="text-white">,</span>}
          <br/>
        </>
      ))}

      &#125;<br/>
    ]
  </pre>
)
}

const contentLayout = (blok) => (
  <div className="col-12 col-md-9 box-light-gray content">
    <Content blok={blok} />
  </div>
)


export default Endpoint;
