import React from "react";
import {transformImage} from "../utils";

const Card = ({card}) => {
  console.log(card.logos)
  return (
    <div className="col-12 col-md-6 col-lg-4 card-icon">
      <div className="card-wrapper">
        <div className="icon bordered">
          <img className="img-fluid" src={card.icon} />
        </div>
        <div className="fixed-height-content bordered">
          <h2 className={`text-${card.title_color}`}>{card.title}</h2>
          {card.paragraph &&
            <p className="mt-3">
              {card.paragraph}
            </p>
          }
        </div>
        {card.logos &&
          <div class="partners bordered pt-4">
             {card.logos.map(logo => <img className="mr-3" src={logo.filename} />)}
          </div>
        }
      </div>
    </div>
  );
}

export default Card;