/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql, Link, withPrefix } from "gatsby"
import "../styles/general.scss";  

import Header from "./internal/header"

const Layout = ({ children, classes, slug, siblings }) => {

  let baseUrl = '';
  if(process.env.URL){
    baseUrl = process.env.URL;
  }
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  const languages = [
    'en',
    'nl'
  ];

  console.log(slug);

  return (
    <>
      <div id="page" className="site">
        <header className="site-header d-flex align-items-center justify-content-between">
          <nav>Nav goes here</nav>
          <nav className="languages">
            {siblings && siblings.map((sibling, id) =>{
              if(sibling.full_slug === '/' && slug ==='home') return;
              if(sibling.full_slug !== slug) 
                return (<Link key={id} to={`${baseUrl}/${sibling.full_slug}`} className="ml-3">{sibling.lang.toUpperCase()}</Link>)
              
            })}
          </nav>
        </header>
        <main id="content" className={`site-content ${classes}`}>
          <div className="container">
            {children}
          </div>
        </main>
      </div>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
