

const path = require('path');
const fs = require('fs');
// import endpoints from './endpoints/connect-prod_v1.1.json';
import getJson from './endpoints/swaggerFormatter';
let services = {
  connect: {},
  core: {}
}



exports.createPages = async({ graphql, actions }) => {
  const { createPage } = actions;

  const connectUrl = process.env.CONNECT_API_URL || 'https://connect.my-clay.com/v1.1/swagger';
  const coreUrl = process.env.CORE_API_URL || 'https://hardware.my-clay.com/v1.2/swagger';

  services.connect = await getJson(connectUrl);
  services.core = await getJson(coreUrl);

  return new Promise( async (resolve, reject) => {

    const storyblokEntry = path.resolve('src/templates/storyblok-entry.js')

    resolve(
      graphql(
        `{
          allStoryblokEntry {
            edges {
              node {
                id
                name
                created_at
                uuid
                slug
                full_slug
                content
                is_startpage
                parent_id
                group_id
              }
            }
          }
        }`
      ).then(result => {
        if (result.errors) {
          
          e.log(result.errors)
          reject(result.errors)
        }


        const entries = result.data.allStoryblokEntry.edges;

        entries.forEach((entry, index) => {

          let pagePath = entry.node.full_slug == 'home' ? '' : `${entry.node.full_slug}/`;

          let story = entry.node;

          const start = story.id.indexOf('-');
          const end = story.id.lastIndexOf('-');
          const id = story.id.substring(start, end);

          const siblings = entries.filter(entry => entry.node.id.includes(id));

          let content = JSON.parse(story.content)

          const parsedSiblings = siblings.map(sibling => {

            let lang = sibling.node.id.substring(sibling.node.id.lastIndexOf('-') + 1, sibling.node.id.length);

            if(lang ==='default') lang = 'en';
            const full_slug = sibling.node.full_slug === 'home' ? '/' : sibling.node.full_slug;

            return {
              lang,
              full_slug
            }
          });

          content.siblings = parsedSiblings;


          if(entry.node.full_slug && entry.node.full_slug.includes('developers')){
            if(content.Endpoint) {
              content.Endpoint.data = getEndpointData(entry.node.content);
            }
          }

          story.content = JSON.stringify(content);

          createPage({
            path: `/${pagePath}`,
            component: storyblokEntry,
            context: {
              story
            }
          });
        })
      })
    )
  })
}

// exports.onCreateNode = ({ node, actions, getNode }) => {
//     const { createNodeField } = actions;

//     // console.log('node: ');console.log(node);
    
//     if(node.path && node.path.includes('endpoint')){
//       console.log('endpoint found');
// };

const getEndpointData = (content) => {
    const contentData = JSON.parse(content);

    const endPointName = contentData.Endpoint.endpoint;
    
    if(!endPointName || !contentData.Endpoint) return;

    const method = contentData.Endpoint.method;
    const service = contentData.Endpoint.service;

    const swaggerObj = JSON.parse(services[service]);
  
    const data = swaggerObj[endPointName][method];

    return data;
}